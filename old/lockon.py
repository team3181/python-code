import numpy as np
import cv2
import time
from collections import deque
from random import randint

cap = cv2.VideoCapture("test.avi")
Width = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
Height = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))
fourcc = cv2.VideoWriter_fourcc('M','J','P','G')
wri = cv2.VideoWriter("e    .avi",fourcc,60,(Width,Height),True)
side = "left"
# cap = cv2.VideoCapture("test.avi")

# declaring radius value

avg = deque(maxlen = 5)
n=1
i=1
radius = 0

def average(radius):
    # if (total - radius))
    avg.append(radius)
    total = sum(avg)
    return (total/5)

r = 1
framerate = 60
# redLower = (80, 230, 75)
# redUpper = (90,255,158)
redLower = (70, 220, 65)
redUpper = (100, 265, 170)
while(True):
    # Capture frame-by-frame
    ret, frame = cap.read()
    # cv2.imwrite("frame.jpg",frame)

    blurred = cv2.GaussianBlur(frame,(11,11),0)
    hsv = cv2.cvtColor(frame,cv2.COLOR_BGR2HSV)

    mask = cv2.inRange(hsv,redLower,redUpper)
    mask = cv2.erode(mask,None,iterations=2)
    mask = cv2.dilate(mask,None,iterations=2)

    cnts = cv2.findContours(mask.copy(),cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE)[-2]

    # frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    center = None

    if len(cnts) > 0:
        c = max(cnts,key=cv2.contourArea)
        ((x,y), radius) = cv2.minEnclosingCircle(c)
        M = cv2.moments(c)
        center = (int(M["m10"] / M["m00"]), int(M["m01"] / M["m00"]))
        
        if (radius > 10):
            cv2.circle(frame,(int(x), int(y)), int(radius),(0,255,255),9)
            cv2.circle(frame,center,5,(0,0,255),-1)
            if n > 5:
                if (((sum(avg))/5)-radius)/sum(avg) < .02:
                    distance = 105*84/average(radius)
                    print (distance)
                    i = 0
                elif i > 10:
                    distance = 105*84/average(radius)
                    print (distance)
                else:
                    i = i+1
                    if i > 11:
                        i=0
            else:
                average(radius)
            #print (radius)
            #print (avg)
            x_pos = x - 320
            print (x_pos)
            if x_pos < 0:
                x_pos = 0-x_pos
                side = "left"
            else:
                side = "right"
    # wri.write(frame)
    # Our operations on the frame come here
    # print (int(radius)*9/80)
    # Display the resulting frame

    font = cv2.FONT_HERSHEY_SIMPLEX
    if (radius > 10):
         if n > 5:
              str_distance = str(distance)
              cv2.putText(frame, "distance: {distance} inches".format(**vars()), (10,20), font, .5, (255,255,255), 1, cv2.LINE_AA)
              cv2.putText(frame, "{x_pos} pixels {side} of center".format(**vars()), (10,50), font, .5, (255,255,255), 1, cv2.LINE_AA)
    cv2.imshow('frame',frame)
    # wri.write(frame)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break
    time.sleep(1 / framerate)
    n = n+1
# When everything done, release the capture
cap.release()
# print (radius)
# print (average(radius))
# print (avg)
cv2.destroyAllWindows()
