# When running, this will continue incrementing the value 'robotTime',

# and the value should be visible to networktables clients such as 

# SmartDashboard. To view using the SmartDashboard, you can launch it

# like so:

#

#     SmartDashboard.jar ip 127.0.0.1

import time
from random import randint
from networktables import NetworkTable



import logging


logging.basicConfig(level=logging.DEBUG)

NetworkTable.setIPAddress('roborio-3181-frc.local')
NetworkTable.setClientMode()
NetworkTable.initialize()


distance = randint(0,9)



table = NetworkTable.getTable("datatable")



while True:
    table.putNumber('Distance', distance)
    print(randint(0,9))
    time.sleep(1)
