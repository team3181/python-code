import numpy as np
import cv2
import time
from collections import deque
# from networktables import NetworkTable
import logging
import datetime
import time


ts = time.time()
st = datetime.datetime.fromtimestamp(ts).strftime('%H:%M:%S')
cap = cv2.VideoCapture(1)
Width = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
Height = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))
fourcc = cv2.VideoWriter_fourcc('M','J','P','G')
wri = cv2.VideoWriter("{st}.avi".format(**vars()),fourcc,60,(Width,Height),True)

# cap = cv2.VideoCapture("test.avi")
# setting up net tables
# logging.basicConfig(level=logging.DEBUG)
#
# NetworkTable.setIPAddress('roborio-3181-frc.local')
# NetworkTable.setClientMode()
# NetworkTable.initialize()
#
# table = NetworkTable.getTable("datatable")
# Defining average function
avg = deque(maxlen = 5)

n=1
i=1

def average(radius):
    avg.append(radius)
    total = sum(avg)
    return (total/5)

# declaring radius value
framerate = 60
# redLower = (80, 230, 75)q
# redUpper = (90,255,158)
redLower = (75, 225, 80)
redUpper = (95, 260, 165)
while(True):
    # Capture frame-by-frame
    ret, frame = cap.read()
    # cv2.imwrite("frame.jpg",frame)
    cv2.imwrite("frame.jpg", frame)

    blurred = cv2.GaussianBlur(frame,(11,11),0)
    hsv = cv2.cvtColor(frame,cv2.COLOR_BGR2HSV)

    mask = cv2.inRange(hsv,redLower,redUpper)
    mask = cv2.erode(mask,None,iterations=2)
    mask = cv2.dilate(mask,None,iterations=2)

    cnts = cv2.findContours(mask.copy(),cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE)[-2]

    # frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    center = None

    if len(cnts) > 0:
        c = max(cnts,key=cv2.contourArea)
        ((x,y), radius) = cv2.minEnclosingCircle(c)
        M = cv2.moments(c)
        center = (int(M["m10"] / M["m00"]), int(M["m01"] / M["m00"]))  
        if (radius >  10):
            cv2.circle(frame,(int(x), int(y)), int(radius),(0,255,255),9)
            cv2.circle(frame,center,5,(0,0,255),-1)
            if n > 5:
                if (((sum(avg))/5)-radius)/sum(avg) < .02:
                    distance = 105*84/average(radius)
                    font = cv2.FONT_HERSHEY_SIMPLEX
                    cv2.putText(frame, "radius: {radius}".format(**vars()), (10,50), font, .5, (255,255,255), 1, cv2.LINE_AA)
                    # table.putNumber('Distance', distance)
                    i = 0
                elif i > 10:
                    distance = 105*84/average(radius)
                    print(distance)
                    # table.putNumber('Distance', distance)
                else:
                    i = i+1
                    if i > 11:
                        i=0
            else:
                average(radius)
            # print ("inches: ",distance, " feet: ", distance/12)
            # print ("radius: ",radius)
    n = n+1
    print(n)
    if n > 60:
        wri.release()
        ts = time.time()
        st = datetime.datetime.fromtimestamp(ts).strftime('%H:%M:%S')
        fourcc = cv2.VideoWriter_fourcc('M','J','P','G')
        wri = cv2.VideoWriter("{st}.avi".format(**vars()),fourcc,60,(Width,Height),True)
        n = 0
    wri.write(frame)
    cv2.imwrite("circle.jpg", frame)
    # Our operations on the frame come here
    # print (int(radius)*9/80)
    # Display the resulting frame
    # cv2.imshow('frame',frame)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break
    time.sleep(1 / framerate)

# When everything done, release the capture
cap.release()
# print (distance)
# print (average(radius))
cv2.destroyAllWindows()

