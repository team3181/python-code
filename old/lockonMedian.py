import numpy as np
import cv2
import time
from collections import deque
from random import randint

cap = cv2.VideoCapture("test.avi")
width = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
height = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))
fourcc = cv2.VideoWriter_fourcc('M', 'J', 'P', 'G')
wri = cv2.VideoWriter("e    .avi", fourcc, 60, (width, height), True)
side = "left"
# cap = cv2.VideoCapture("test.avi")

# declaring radius value

queueLength = 10
queue = []
n = 1
i = 1
radius = 0

# Add items to queue until reaching queueLength
while len(queue) <= queueLength:
    queue.append(radius)


def average(radius):
    # if (total - radius))
    queue.append(radius)


r = 1
framerate = 60
greenLower = (75, 225, 80)
greenUpper = (95, 260, 165)

while (True):
    # Capture frame-by-frame
    ret, frame = cap.read()
    # cv2.imwrite("frame.jpg",frame)

    # Image operations to
    blurred = cv2.GaussianBlur(frame, (11, 11), 0)
    hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
    mask = cv2.inRange(hsv, greenLower, greenUpper)
    mask = cv2.erode(mask, None, iterations=2)
    mask = cv2.dilate(mask, None, iterations=2)

    cnts = cv2.findContours(mask.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)[-2]

    # frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    center = None

    if len(cnts) > 0:
        c = max(cnts, key=cv2.contourArea)
        ((x, y), radius) = cv2.minEnclosingCircle(c)
        M = cv2.moments(c)
        center = (int(M["m10"] / M["m00"]), int(M["m01"] / M["m00"]))
        queue.append(radius)
        queue.pop(0)
        sortedQueue = sorted(queue)
        median = sortedQueue[queueLength / 2]
        if (radius > 10):
            cv2.circle(frame, (int(x), int(y)), int(radius), (0, 255, 255), 9)
            cv2.circle(frame, center, 5, (0, 0, 255), -1)
            distance = 105 * 84 / median
            x_pos = x - 320
            print (x_pos)
            if x_pos < 0:
                x_pos = 0 - x_pos
                side = "left"
            else:
                side = "right"
    # Display the resulting frame
    font = cv2.FONT_HERSHEY_SIMPLEX
    if (radius > 10):
        if n > 5:
            if (distance):
                str_distance = str(distance)
                cv2.putText(frame, "distance: {distance} inches".format(**vars()), (10, 20), font, .5, (255, 255, 255), 1, cv2.LINE_AA)
                cv2.putText(frame, "{x_pos} pixels {side} of center".format(**vars()), (10, 50), font, .5, (255, 255, 255), 1, cv2.LINE_AA)
            else:
                print("[ERR] Variable: DISTANCE not defined");
    cv2.imshow('frame', frame)
    # wri.write(frame)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break
    time.sleep(1 / framerate)
    n += 1
# When everything done, release the capture and close real-time viewing window
cap.release()
cv2.destroyAllWindows()