#!/usr/bin/env python3
#
# This is a NetworkTables client (eg, the DriverStation/coprocessor side).
# You need to tell it the IP address of the NetworkTables server (the
# robot or simulator).
#
# When running, this will continue incrementing the value 'dsTime', and the
# value should be visible to other networktables clients and the robot.
#

import time
from networktables import NetworkTables

# To see messages from networktables, you must setup logging
import logging
logging.basicConfig(level=logging.DEBUG)

ip = "10.121.0.180"

NetworkTables.setIPAddress(ip)
NetworkTables.setClientMode()
NetworkTables.initialize()

sd = NetworkTables.getTable("SmartDashboard")

dsTime = 0
while True:
    try:
        print'robotTime:', sd.getNumber('robotTime')
        print'robotDistance:', sd.getNumber('robotDistance')
        print'robotAngle:', sd.getNumber('robotAngle')
        print'robotCenter:', sd.getNumber('robotCenter')
    except KeyError:
        print'You\'re bad'

    sd.putNumber('dsTime', dsTime)
    time.sleep(1)
    dsTime += 1
