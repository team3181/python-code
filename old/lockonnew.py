import cv2
import time

cap = cv2.VideoCapture('test.avi')

# Defining green ranges  
greenLower = (75, 225, 80)
greenUpper = (95, 260, 165)

# Adding queue and setting values equal to 0
queueLength = 10
queue = []

while (True):

    # Convert the video into frames, cut out all colors other than grren,
    # then convert it to grayscale, blur it, and threshold the image
    ret, frame = cap.read(0)
    hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
    mask = cv2.inRange(hsv, greenLower, greenUpper)
    blurred = cv2.GaussianBlur(mask, (11, 11), 0)
    thresh = cv2.threshold(blurred, 50, 255, cv2.THRESH_BINARY)[1]

    # Find contours (shapes) in the image
    cnts = cv2.findContours(thresh.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    cnts = cnts[1]

    # Only preform these functions if there are countours
    if len(cnts) > 0:

        # Loop over the contours
        for c in cnts:

            # Compute the center of the contours, as long as they aren't 0
            M = cv2.moments(c)
            if M["m10"] > 0 and M["m00"] > 0 and M["m01"] > 0 and M["m00"] > 0:
                cX = int(M["m10"] / M["m00"])
                cY = int(M["m01"] / M["m00"])

                # Draw the contour and center of the shape on the video
                cv2.drawContours(frame, [c], -1, (0, 255, 0), 2)
                cv2.circle(frame, (cX, cY), 7, (255, 255, 255), -1)

    # show the image
    cv2.imshow("Frame", frame)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break
    time.sleep(1 / 60)
