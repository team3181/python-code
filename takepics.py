import cv2
import time
from operator import itemgetter
import logging
from subprocess import call
import os

call(["v4l2-ctl","--set-ctrl=exPosure_auto=1"])
call(["v4l2-ctl","--set-ctrl=exPosure_absolute=5"])

logging.basicConfig(level=logging.DEBUG)

# Set variables
cap = cv2.VideoCapture(0)
framerate = 60
img_number = 0
dirname = 'test'
try:
	os.mkdir(dirname)
except:
	print("folder exists")

while True:
		# Stop loop if press q
		if cv2.waitKey(1) & 0xFF == ord('q'):
			exit()

		ret, frame = cap.read()

		cv2.imwrite(os.path.join(dirname, "output" + str(img_number)+ ".jpg"), frame)
		print("output" + str(img_number)+ ".jpg")
		img_number += 1

		# Show the frame
		cv2.imshow("Frame", frame)

		# Wait 1 frame
		time.sleep(1/float(framerate))
