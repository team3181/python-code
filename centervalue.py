import cv2
import time
from operator import itemgetter
from networktables import NetworkTables
import logging
from subprocess import call

call(["v4l2-ctl","--set-ctrl=exposure_auto=1"])
call(["v4l2-ctl","--set-ctrl=exposure_absolute=5"])

H = []
S = []
V = []

# Get version of cv2 (Important for ensuring compatibility with 2.4.13.2 and 3.1.0)
version = cv2.__version__

# Set capture to our video input
cap = cv2.VideoCapture(0)

if version == "2.4.13.2":
	width = cap.get(cv2.cv.CV_CAP_PROP_FRAME_WIDTH)
	height = cap.get(cv2.cv.CV_CAP_PROP_FRAME_HEIGHT)
elif version == "3.1.0":
	width = cap.get(cv2.CAP_PROP_FRAME_WIDTH)
	height = cap.get(cv2.CAP_PROP_FRAME_HEIGHT)
else:
	print "Version not supported"

cX = width/2
cY = height/2

# Camera settings
framerate = 60

while True:
		
		# Convert the video into frames, cut out all colors other than green,then convert it to grayscale, blur it, and threshold the frame
		ret, frame = cap.read()
		hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
		blurred = cv2.GaussianBlur(hsv, (15, 15), 0)                                                            

		c = blurred[int(cY),int(cX)]
		centerValueBGR = frame[int(cY),int(cX)]

		if cv2.waitKey(1) & 0xFF == ord('y'):
			# cv2.imwrite("output.jpg", frame)
                        H.append(c[0])
                        S.append(c[1])
                        V.append(c[2])
                        if len(H) > 2:
                            H = sorted(H)
                            S = sorted(S)
                            V = sorted(V)
                            H.pop(1)
                            S.pop(1)
                            V.pop(1)
                        print "IM DOIN STUFF"
                # Stop loop if press q
		if cv2.waitKey(1) & 0xFF == ord('r'):
			break
		# Move cursor with arrow keys
		if cv2.waitKey(1) & 0xFF == ord('t'):
			cY -= 10
		if cv2.waitKey(1) & 0xFF == ord('g'):
			cY += 10
		if cv2.waitKey(1) & 0xFF == ord('f'):
			cX -= 10
                if cv2.waitKey(1) & 0xFF == ord('h'):
			cX += 10
		# Save image if press s
                        
		cv2.circle(frame, (int(cX), int(cY)), 2, (255, 255, 255), -1)
		
		# Show the frame
		cv2.imshow("Frame", frame)

		# Wait 1 frame
		time.sleep(1/float(framerate))

print "H: " + str(H)
print "S: " + str(S)
print "V: " + str(V)
