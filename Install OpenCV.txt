THIS WILL TAKE A LONG TIME (POSSIBLY MULTIPLE HOURS).
IF YOU INTERRUPT THE INSTALL, YOU RISK HAVING TO START OVER.

----------------------------------------------------------------------------------------------------

Raspberry Pi:

    Tutorial:

        Install Raspbian with NOOBS by following the instructions here: https://www.raspberrypi.org/downloads/noobs/

        SSH into the pi (optional, but easier in my opinion):

            ssh pi@ipAddress
            password: raspberry

        Commands:

            NOTE THAT IF THERE IS A NEWER VERSION OF OPENCV, THE VERSION NUMBER (3.2.0) MUST BE CHANGED TO THE NEW VERSION NUMBER IN THE COMMANDS BELOW

            sudo apt-get update
            sudo apt-get upgrade
            sudo rpi-update
            sudo reboot

            sudo apt-get install build-essential cmake pkg-config
            sudo apt-get install libjpeg-dev libtiff5-dev libjasper-dev libpng12-dev
            sudo apt-get install libavcodec-dev libavformat-dev libswscale-dev libv4l-dev
            sudo apt-get install libxvidcore-dev libx264-dev
            sudo apt-get install libgtk2.0-dev
            sudo apt-get install libatlas-base-dev gfortran
            sudo apt-get install python2.7-dev

            wget -O opencv.zip https://github.com/Itseez/opencv/archive/3.2.0.zip
            unzip opencv.zip
            wget -O opencv_contrib.zip https://github.com/Itseez/opencv_contrib/archive/3.2.0.zip
            unzip opencv_contrib.zip
            wget https://bootstrap.pypa.io/get-pip.py
            sudo python get-pip.py
            sudo rm -rf ~/.cache/pip
            sudo pip install numpy
            sudo pip install pynetworktables
            cd ~/opencv-3.2.0/
            mkdir build
            cd build
            cmake -D CMAKE_BUILD_TYPE=RELEASE \
                    -D CMAKE_INSTALL_PREFIX=/usr/local \
                    -D INSTALL_PYTHON_EXAMPLES=OFF \
                    -D OPENCV_EXTRA_MODULES_PATH=~/opencv_contrib-3.2.0/modules \
                    -D BUILD_EXAMPLES=ON ..
            make -j4
            sudo make install
            sudo ldconfig
            cd lib
            sudo ln -s cv2.so /usr/local/lib/python2.7/site-packages/cv2.so

    Helpful Resources:

        http://www.pyimagesearch.com/2016/04/18/install-guide-raspberry-pi-3-raspbian-jessie-opencv-3/

----------------------------------------------------------------------------------------------------

macOS:

    Tutorial:

        Install Xcode from the App Store
        Accept the license by running "sudo xcodebuild -license" in Terminal
        Install Apple Command Line Tools by running "sudo xcode-select --install" in Terminal

        Install Homebrew by following the instructions here: https://brew.sh

        Commands:

            brew install python
            brew linkapps python
            brew tap homebrew/science
            brew install opencv3 --with-contrib
            echo /usr/local/opt/opencv3/lib/python2.7/site-packages >> /usr/local/lib/python2.7/site-packages/opencv3.pth

    Helpful Resources:

        http://seeb0h.github.io/howto/howto-install-homebrew-python-opencv-osx-el-capitan/
        http://www.pyimagesearch.com/2016/12/05/macos-install-opencv-3-and-python-3-5/
        http://www.pyimagesearch.com/2016/12/19/install-opencv-3-on-macos-with-homebrew-the-easy-way/

----------------------------------------------------------------------------------------------------

Verify it’s installed:

    Commands:

        python
        import cv2
        cv2.__version__

- cjacobson
