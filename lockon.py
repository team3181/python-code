import cv2
import time
from operator import itemgetter
from networktables import NetworkTables
import logging
from subprocess import call

call(["v4l2-ctl","--set-ctrl=exPosure_auto=1"])
call(["v4l2-ctl","--set-ctrl=exPosure_absolute=5"])

logging.basicConfig(level=logging.DEBUG)
NetworkTables.initialize(server="roborio-3181-frc.local")
sd = NetworkTables.getTable("CameraData")

# Get version of cv2 (Important for ensuring compatibility with 2.4.13.2 and 3.1.0)
version = cv2.__version__

# Set constants
pi = 3.1415926

# Set capture to our video input
cap = cv2.VideoCapture(0)
# cap = cv2.VideoCapture("output.avi")

# Set height and width (Done differently based on which version of cv2 is used)
if version == "2.4.13.2":
	width = cap.get(cv2.cv.CV_CAP_PROP_FRAME_WIDTH)
	height = cap.get(cv2.cv.CV_CAP_PROP_FRAME_HEIGHT)
elif version == "3.1.0":
	width = cap.get(cv2.CAP_PROP_FRAME_WIDTH)
	height = cap.get(cv2.CAP_PROP_FRAME_HEIGHT)
else:
	print("Version not supported")

# Defining green ranges.
# OpenCV uses [0,179] for Hue, [0,255] for Saturation, and [0,255] for Value
HSV = [78, 222, 232]
HSVVariance = [30,40,50]
greenLower = (HSV[0] - HSVVariance[0], HSV[1] - HSVVariance[1], HSV[2] - HSVVariance[2])
greenUpper = (HSV[0] + HSVVariance[0], HSV[1] + HSVVariance[1], HSV[2] + HSVVariance[2])

# Camera settings
framerate = 60

sd.putBoolean("cameraGo", False)
while True:
	sd.putBoolean('isWorking', False)
	while(sd.getBoolean('cameraGo') == True):
		sd.putBoolean('isWorking', True)

		# Stop loop if press q
		if cv2.waitKey(1) & 0xFF == ord('q'):
			exit()
		# Slow down if press 1
		if cv2.waitKey(1) & 0xFF == ord('1'):
			framerate = 10
		# Return to normal speed if press 2
		if cv2.waitKey(1) & 0xFF == ord('2'):
			framerate = 60
		# Save image if press s
		if cv2.waitKey(1) & 0xFF == ord('s'):
			cv2.imwrite("output.jpg", frame)

		# Convert the video into frames, cut out all colors other than green,then convert it to grayscale, blur it, and threshold the frame
		ret, frame = cap.read()
		hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
		mask = cv2.inRange(hsv, greenLower, greenUpper)
		blurred = cv2.GaussianBlur(mask, (15, 15), 0)
		thresh = cv2.threshold(blurred, 50, 255, cv2.THRESH_BINARY)[1]

		# Find contours (shapes) in the frame
		cnts = cv2.findContours(thresh.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
		if version == "2.4.13.2":
			cnts = cnts[0]
		elif version == "3.1.0":
			cnts = cnts[1]

		# Only preform these functions if there are countours
		if len(cnts) > 0:

			contourInfo = []
			contourInfoList = []
			sortedContourInfoList = []

			# Loop over the contours
			for c in cnts:

				# Compute the center and area of the contours, as long as they aren't 0
				M = cv2.moments(c)
				if M["m00"] > 0:

					cX = int(M["m10"] / M["m00"])
					cY = int(M["m01"] / M["m00"])
					area = cv2.contourArea(c)

					# Make lists of centers and area,
					# then put that information in a list and sort it by area
					contourInfo = [cX,cY,area]
					contourInfoList.append(contourInfo)
					sortedContourInfoList = sorted(contourInfoList, key=itemgetter(2), reverse = True)

					# Draw the contour and center of the shape on the video
					cv2.drawContours(frame, [c], -1, (255, 0, 255), 2)
					cv2.circle(frame, (cX, cY), 4, (255, 255, 255), -1)

			# If there are multiple contours, get the centers of the two largest
			if len(sortedContourInfoList) > 1:

				X1 = sortedContourInfoList[0][0]
				Y1 = sortedContourInfoList[0][1]
				X2 = sortedContourInfoList[1][0]
				Y2 = sortedContourInfoList[1][1]

				# Find the point between the two centers, then draw it, and calculate distance from center of the frame
				avgX = (X1+X2)/2
				avgY = (Y1+Y2)/2
				cv2.circle(frame, (avgX, avgY), 4, (0, 0, 255), -1)
				xPos = avgX - width/2
				cv2.putText(frame,"X Position: " + str(xPos), (0,15), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255,255,255))
				sd.putNumber("robotCenter", xPos)

				# Convert xPos to angle
				angleMult = .08
				angleDeg = angleMult * xPos
				angleRad = ((pi) / 180) * angleDeg
				cv2.putText(frame,"AngleRad: " + str(angleRad), (0,35), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255,255,255))
				sd.putNumber("robotAngleRad", angleRad)
				cv2.putText(frame,"AngleDeg: " + str(angleDeg), (250,35), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255,255,255))
				sd.putNumber("robotAngleDeg", angleDeg)

				# Calculate distance between two centers (in pixels)
				pixelDistance = ((X1-X2)**2 + (Y1-Y2)**2)**.5
				cv2.putText(frame,"Pixel Distance: " + str(pixelDistance), (0,55), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255,255,255))
				sd.putNumber("robotPixelDistance", pixelDistance)

				# Convert the distance between the two centers to distance from the peg/wall
				distanceMult = 14500
				physicalDistance = distanceMult / pixelDistance
				cv2.putText(frame,"Physical Distance: " + str(physicalDistance), (0,75), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255,255,255))
				sd.putNumber("robotPhysicalDistance", physicalDistance)

		# Show the frame
		# cv2.imshow("Frame", frame)

		# Wait 1 frame
		time.sleep(1/float(framerate))
